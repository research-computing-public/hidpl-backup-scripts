SERVER_FQDN='hidpl-data01.stanford.edu'
SRC_PATH='/data/miseq/.'
BACKUP_PATH="/archive/zfs01/$(date +%B)/hidpl-data01/data/miseq"
MAIL_TO='akkornel@stanford.edu'

DEBUG=1
MAIL_ON_COMPLETE=1

LOCK_PATH="/tmp/backup.$SERVER_FQDN.lock"
LOG_PATH=$(mktemp /tmp/backup.$SERVER_FQDN.XXXXXXXX.txt)

# Trap exit in order to clean up our log file
cleanup() {
	rm $LOG_PATH
}
trap cleanup EXIT

# Make sure we can write to the lock file
make_lock() {
	if [[ -e $LOCK_PATH && ! -w $LOCK_PATH ]] ; then
		echo 'The file already exists.  This is likely a permissions issue.' | mail -s "Unable to write to backup lock $LOCK_PATH" $MAIL_TO
		return 0
	fi

	touch $LOCK_PATH
	if [ $? -ne 0 ]; then 
		echo 'The file did not already exist.  This is likely a permissions issue, or a parent directory may be missing.' | mail -s "Could not create backup lock $LOCK_PATH" $MAIL_TO
		return 0
	fi

	return 1
}

# If we can't write to or create the lock file, then exit.
[ $DEBUG -eq 1 ] && echo 'Making lock file'
make_lock
make_lock_result=$?
[ $DEBUG -eq 1 ] && echo "Result is $make_lock_result"
[ $make_lock_result -eq 0 ] && exit 1

# Connect to our log file, and then try for a lock
# We'll wait up to 5 minutes to grab it
[ $DEBUG -eq 1 ] && echo 'Taking lock'
exec 3>> $LOCK_PATH
flock --exclusive --timeout 300 3
if [ $? -ne 0 ]; then
	echo 'Lock failed or timed out!'
	echo 'Tried waiting 5 minutes' | mail -s "Gave up waiting for backup lock on $LOCK_PATH" $MAIL_TO
	exit 1
fi
[ $DEBUG -eq 1 ] && echo 'Got the lock!'

# Create the destination directory (if needed), and do the backup!
test -d $BACKUP_PATH || mkdir -p $BACKUP_PATH
echo 'Running the backup!'
echo -n 'Backup started on ' >> $LOG_PATH
date >> $LOG_PATH
rsync -aHAX -r -x -v root@$SERVER_FQDN:$SRC_PATH $BACKUP_PATH/. >> $LOG_PATH 2>&1
echo 'Program finished on ' >> $LOG_PATH
date >> $LOG_PATH

# See if the backup worked
if [ $? -eq 0 ]; then
	echo 'Backup complete!'
	[ $MAIL_ON_COMPLETE -eq 1 ] &&
	echo 'Wooo!' | mail -s "Backup for $SERVER_FQDN Complete!" $MAIL_TO

# If the backup had a problem, send a mail about it.
else
	echo 'Problem!'
	echo "Log file in $LOG_PATH" | mail -s "Backup problem with $SERVER_FQDN" $MAIL_TO
	echo "Log file from $LOG_PATH attached!" | mail -a $LOG_PATH -s "Backup problem with $SERVER_FQDN" $MAIL_TO
fi

# All done!
# (the log file will be cleaned up automatically)
exit 0
